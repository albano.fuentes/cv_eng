import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.css']
})
export class DatosPersonalesComponent implements OnInit {

  public fechaNacimiento: string = '21/12/1987';
  public pais: string = 'CABA';
  // public direccion: string = 'Apolinario Figueroa 331';
  public codigoPostal: string = 'Argentina';
  public telefono: string = '+54 9 1155175150';
  public mail: string = 'albano.fuentes@gmail.com';

  constructor() { }

  ngOnInit(): void {
  }

}
