import { Component, OnInit } from '@angular/core';
import { ExperienceService } from '../../servicios/experience.service';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css']
})
export class ExperienceComponent implements OnInit {

  Experiences: any[];

  constructor( private experienceService: ExperienceService) {
  }

  ngOnInit(): void {
    this.Experiences = this.experienceService.getExperiences();
  }

}
