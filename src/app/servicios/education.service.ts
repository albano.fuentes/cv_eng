import { Injectable } from '@angular/core';


@Injectable()
export class EducationService {

    constructor() {}

    private Edus: Edu[] = [
        {
            id: 1,
            titulo : 'Systems Engineering',
            periodo: '03/2019 - Present - In Progress',
            institucion : 'U.T.N / F.R.B.A.',
            ciudadInstitucion : 'CABA',
            paisInstitucion : 'Argentina',
            tecnos: []
        },
        {
            id: 2,
            titulo : 'Electronic Engineering',
            periodo: '03/2012 - 12/2016 - Incomplete',
            institucion : 'U.T.N / F.R.B.A.',
            ciudadInstitucion : 'CABA',
            paisInstitucion : 'Argentina',
            tecnos: []
        },
        {
            id: 3,
            titulo : 'Electronic Technician',
            periodo: '03/2001 - 12/2006',
            institucion : 'Technical school N°1 "Ing. Otto Krause',
            ciudadInstitucion : 'CABA',
            paisInstitucion : 'Argentina',
            tecnos: []
        },
        {
            id: 4,
            titulo : 'Web App Full Stack',
            periodo: '03/2019 - 05/2019',
            institucion : 'Udemi',
            ciudadInstitucion : 'CABA',
            paisInstitucion : 'Argentina',
            tecnos: [
                'Front End Development with Angular 8 and Back End Spring 5, Spring Boot, REST API, JPA.',
            ]
        },
        {
            id: 5,
            titulo : 'Web App Full Stack',
            periodo: '04/2019 - 04/2019',
            institucion : 'Udemi',
            ciudadInstitucion : 'CABA',
            paisInstitucion : 'Argentina',
            tecnos: [
                'Front End development with Angular 9 and Back End NodeJS ECMAScript 6, using good practices.',
            ]
        }
    ];

    getEdus() {
        return this.Edus;
    }
}

interface Edu {
    id: number;
    titulo: string;
    periodo: string;
    institucion: string;
    ciudadInstitucion: string;
    paisInstitucion: string;
    tecnos: string[];
}
