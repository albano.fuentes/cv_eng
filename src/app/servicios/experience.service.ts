import { Injectable } from '@angular/core';


@Injectable()
export class ExperienceService {

    constructor() {}

    private Experiences: Experiences[] = [
        {
            id: 0,
            cargo: 'Full Stack Developer',
            periodo: '08/2020 - Present',
            empresa: 'Grupo Banco Nación(National Bank Group).',
            ciudadEmpresa: 'CABA',
            paisEmpresa: 'Argentina',
            tareas: [
                'Development, design and implementation of CRUD in SPA with React JS, Express JS and Node JS.',
                'Communication between applications through Pub / Sub and cloud functions with Google Cloud.',
                'Participation in multiple SUBE projects (the only national public transport payment method).',
            ]
        },
        {
            id: 1,
            cargo: 'Full Stack Developer',
            periodo: '11/2019 - 08/2020',
            empresa: 'Neoris S.A.',
            ciudadEmpresa: 'CABA',
            paisEmpresa: 'Argentina',
            tareas: [
                'Development, design and implementation of SPA in Angular and Spring.',
                'Back End support of JSP, STRUT, Spring to banks.'
            ]
        },
        {
            id: 2,
            cargo: 'Java Developer',
            periodo: '03/2019 - 06/2019',
            empresa: 'Freelance',
            ciudadEmpresa: 'CABA',
            paisEmpresa: 'Argentina',
            tareas: [
                'Collaborating with other developers in the design, development, testing and implementation of new JSP pages and Servlets.',
                'Website maintenance using own knowledge and experience solving any problem.',
                'Providing web support and advice to clients.'
            ]
        },
        {
            id: 3,
            cargo: 'Clock support',
            periodo: '10/2018 - 06/2019',
            empresa: 'Coto C.I.C.S.A.',
            ciudadEmpresa: 'CABA',
            paisEmpresa: 'Argentina',
            tareas: [
                'Personnel time control equipment and access controls fabrication.',
                'Back Office development for setting clocks.',
                'Communication with the customer.',
                'Support and development of new functionalities in existing projects.'
            ]
        },
    ];

    getExperiences() {
        return this.Experiences;
    }
}

interface Experiences {
    id: number;
    cargo: string;
    periodo: string;
    empresa: string;
    ciudadEmpresa: string;
    paisEmpresa: string;
    tareas: string[];
}

